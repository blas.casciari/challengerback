<?php

class ConexionBBDD
{
    private $servername;
    private $username;
    private $password;
    private $database;
    private $socket;
    private $port;
    private $conn;

    public function __construct($servername, $username, $password, $database, $socket, $port = 3306)
    {
        $this->servername = $servername;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->socket = $socket;
        $this->port = $port;

        $this->conectar();
    }

    /**
     * Conectar a la base de datos
     * @return PDO
     * @throws PDOException
     */
    public function conectar()
    {
        try {
            // Crear una conexión PDO con la ruta del socket y el puerto
            $conexion = new PDO("mysql:host=$this->servername;port=$this->port;dbname=$this->database;unix_socket=$this->socket", $this->username, $this->password);

            // Establecer el modo de error para excepciones
            $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $conexion;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Obtener la base de datos
     */
    public function getConexion()
    {
        return $this->conn;
    }

    /**
     * Cierre de la base de datos
     * */
    public function cerrarConexion()
    {
        $this->conn = null;
    }

    /**
     * Ejecutar una consulta
     * @param PDO $conn
     * @param string $sql
     * @return array|bool
     * @throws PDOException
     */
    public function ejecutarConsulta($conexionBD, $sql)
    {
        try {
            $conn = $conexionBD->conectar();
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Error en la consulta: " . $e->getMessage();
            return false;
        }
    }
}
