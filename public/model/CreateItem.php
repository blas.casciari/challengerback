<?php
include "model/conexionBBDD.php";

class CreateItem
{
    const LAST_VALUE_DOLAR = 950;

    private $conexionBD;

    public function __construct()
    {
        $this->conexionBD = new conexionBBDD("mysql", "user", "123", "product", "/var/run/mysqld/mysqld.sock", 3306);
    }

    /**
     * handleCreate
     * 
     * funcion que verifica que no se manden campos vacios.
     * y crea el item en la base de datos.
     * 
     * @param string $producto
     * @param double $precio
     */
    public function handleCreate($producto = null, $precio = null)
    {
        if (empty($producto) || empty($precio)) {
            throw new Exception('Se requieren campos nombre y precio');
        }

        $this->create($this->conexionBD, $producto, $precio);
        return json_encode(['success' => 'Item creado con éxito']);
    }

    /**
     * funcion que crea el item
     * 
     * @param object $conn
     * @param string $producto
     * @param double $precio
     * 
     */
    public function create($conn, $producto, $precio)
    {
        try {
            $sql = "INSERT INTO product (nombre_producto, precio_pesos, estado) VALUES ('$producto', $precio, 1)";
            $conn->ejecutarConsulta($conn, $sql);
            $this->conexionBD->cerrarConexion();
        } catch (\Throwable $th) {
            echo json_encode(['error' => $th->getMessage()]);
        }
    }

    /**
     * Funcion verifica que el campo ID no este vacio antes de ejecutar la actualizacion
     * 
     * @param int $id
     * @param string $producto
     * @param double $precio
     * @param bool $status
     * 
     */
    public function handleUpdate($id, $producto, $precio, $status)
    {
        if (empty($id)) {
            throw new Exception('Se requiere el ID');
        }

        $this->update($this->conexionBD, $id, $producto, $precio, $status);
        return json_encode(['success' => 'Item se actualizó con éxito']);
    }

    /**
     * Funcion que actualiza el item
     * 
     * @param object $conn
     * @param int $id
     * @param string $producto
     * @param double $precio
     * @param bool $status
     * 
     */
    public function update($conn, $id, $producto, $precio, $status)
    {
        try {
            if (!empty($producto)) {
                $sql = "UPDATE product SET nombre_producto = '$producto' WHERE id = $id";
                $conn->ejecutarConsulta($conn, $sql);
            }

            if (!empty($precio)) {
                $sql = "UPDATE product SET precio_pesos = $precio WHERE id = $id";
                $conn->ejecutarConsulta($conn, $sql);
            }

            if (isset($status)) {
                $sql = "UPDATE product SET estado = $status WHERE id = $id";
                $conn->ejecutarConsulta($conn, $sql);
            }

            $this->conexionBD->cerrarConexion();
        } catch (\Throwable $th) {
            echo json_encode(['error' => $th->getMessage()]);
        }
    }

    /**
     * Funcion verifica que el campo ID no este vacio antes de ejecutar la actualizacion
     * @param int $id
     * 
     * */
    public function handleDelete($id)
    {
        if (empty($id)) {
            throw new Exception('Se requiere el ID');
        }

        $this->delete($this->conexionBD, $id);
        return json_encode(['success' => 'Item se eliminó con éxito']);
    }

    /**
     * Funcion que elimina el item
     * 
     * @param object $conn
     * @param int $id
     * 
     * */
    public function delete($conn, $id)
    {
        try {
            $sql = "DELETE FROM product WHERE id = $id";
            $conn->ejecutarConsulta($conn, $sql);
            $this->conexionBD->cerrarConexion();
        } catch (\Throwable $th) {
            echo json_encode(['error' => $th->getMessage()]);
        }
    }

    /**
     * esta funcion se encarga de preparar los datos para crear la vista
     * 
     * @param int $id
     * @param string $producto
     * @param double $dolar
     * @param bool $status
     * 
     * */
    public function handleView($id, $producto, $dolar = null, $status = null)
    {
        return $this->view($this->conexionBD, $id, $producto, $dolar, $status);
    }

    /**
     * Funcion que prepara los datos para la vista
     * 
     * @param object $conn
     * @param int $id
     * @param string $producto
     * @param double $dolar
     * @param bool $status
     * 
     * */
    public function view($conn, $id = null, $producto = null, $dolar = null, $status = null)
    {
        try {
            $sql = "SELECT * FROM product";

            if (!empty($id)) {
                $sql .= " WHERE id = $id";
            }

            if (!empty($producto)) {
                $sql .= empty($id) ?
                    " WHERE nombre_producto LIKE '%$producto%'" :
                    " OR nombre_producto LIKE '%$producto%'";
            }

            if (isset($status)) {
                $sql .= empty($id) ?
                    " WHERE estado = $status" :
                    " OR estado = $status";
            }

            $dolar = empty($dolar) ? self::LAST_VALUE_DOLAR : $dolar;

            $values = [];
            foreach ($conn->ejecutarConsulta($conn, $sql) as $value) {
                $values[] = [
                    'id' => $value['id'],
                    'nombre' => $value['nombre_producto'],
                    'precio_ARS' => number_format($value['precio_pesos'], 2),
                    'precio_USD' => number_format($value['precio_pesos'] / $dolar, 2),
                    'estado' => $value['estado'] ? 'Activo' : 'Inactivo',
                ];
            }

            $this->conexionBD->cerrarConexion();
            return json_encode($values);
        } catch (\Throwable $th) {
            echo json_encode(['error' => $th->getMessage()]);
        }
    }

    /**
     * Esta funcion se encarga de crear la tabla en la base de datos
     * */
    public function table()
    {
        $sql = "CREATE TABLE product (id INT AUTO_INCREMENT PRIMARY KEY, nombre_producto VARCHAR(255) NOT NULL, precio_pesos DECIMAL(10,2) NOT NULL, estado TINYINT(1) DEFAULT 1)";
        return $this->conexionBD->ejecutarConsulta($this->conexionBD, $sql);
    }
}
