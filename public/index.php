<?php
header("Content-Type: application/json");

$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'], '/'));

include_once 'model/CreateItem.php';

$model = new CreateItem();
/**
 * Esta vista se encarga de ejecutar la funcion correspondiente a cada metodo que se envia desde la consulta
 * @var string $method
 * @var array $request
 * @var object $model
 * @var array $values
 */
try {
    switch ($method) {
        case 'POST':
            echo $model->handleCreate($_GET['producto'], $_GET['precio']);
            break;
        case 'PUT':
            echo $model->handleUpdate($_GET['id'], $_GET['producto'], $_GET['precio'], $_GET['status']);
            break;
        case 'DELETE':
            echo $model->handleDelete($_GET['id']);
            break;
        case 'GET':
            echo $model->handleView($_GET['id'], $_GET['producto'], $_GET['dolar'], $_GET['status']);
            break;
        case 'TABLE':
            echo $model->table();
            break;
        default:
            echo json_encode(['error' => 'No se ha podido realizar la peticion.']);
            break;
    }
} catch (Exception $e) {
    return json_encode(['error' => $e->getMessage()]);
}
